#ifndef _TYPES_
#define _TYPES_


#include <cstdint>

using i32 = int32_t;
using ui16 = uint16_t;
using ui32 = uint32_t;

#endif 