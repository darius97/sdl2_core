#include "types.hpp"

#include "sdl_core/window/sdl_window.hpp"
#include "sdl_core/texture/texture.hpp"

#include "tetris/game_proc/game_proc.hpp"

#include <thread>
#include <memory>

i32 main() { 
    sdl_core::SDLWindow window(
        "__TETRISCRAFT__", tetris::window::WIDTH, tetris::window::HEIGHT
        ); 
//    SDL_Texture *txr_menu = txr_manager.load_texture(pathMenu, render);
//    SDL_Texture *txr_new_gameb = txr_manager.load_texture(pathNewGameB, render);
//    SDL_Texture *txr_new_gamer = txr_manager.load_texture(pathNewGameR, render);
//    SDL_Texture *txr_settingsb = txr_manager.load_texture(pathSettingB, render);
//    SDL_Texture *txr_settingsr = txr_manager.load_texture(pathSettingR, render);
//    SDL_Texture *txr_statisticb = txr_manager.load_texture(pathStatisticsB, render);
//    SDL_Texture *txr_statisticr = txr_manager.load_texture(pathStatisticsR, render);
//    SDL_Texture *txr_exitb = txr_manager.load_texture(pathExitB, render);
//    SDL_Texture *txr_exitr = txr_manager.load_texture(pathExitR, render);
//    SDL_Texture *txr_game_over = txr_manager.load_texture(pathGameOver, render);

/*  ButtonPosition newGame;
    newGame.xLeft = BUTTON_CENTER_POSITION_X_LEFT;
    newGame.xRight = BUTTON_CENTER_POSITION_X_RIGHT;
    newGame.yHigh = OFFSET_1 * BUTTON_HEIGHT;
    newGame.yLow = OFFSET_1 * BUTTON_HEIGHT + BUTTON_HEIGHT;
    ButtonPosition setting;
    setting.xLeft = BUTTON_CENTER_POSITION_X_LEFT;
    setting.xRight = BUTTON_CENTER_POSITION_X_RIGHT;
    setting.yHigh = OFFSET_2 * BUTTON_HEIGHT;
    setting.yLow = OFFSET_2 * BUTTON_HEIGHT + BUTTON_HEIGHT;
    ButtonPosition statistics;
    statistics.xLeft = BUTTON_CENTER_POSITION_X_LEFT;
    statistics.xRight = BUTTON_CENTER_POSITION_X_RIGHT;
    statistics.yHigh = OFFSET_3 * BUTTON_HEIGHT;
    statistics.yLow = OFFSET_3 * BUTTON_HEIGHT + BUTTON_HEIGHT;
    ButtonPosition exit;
    exit.xLeft = BUTTON_CENTER_POSITION_X_LEFT;
    exit.xRight = BUTTON_CENTER_POSITION_X_RIGHT;
    exit.yHigh = OFFSET_4 * BUTTON_HEIGHT;
    exit.yLow = OFFSET_4 * BUTTON_HEIGHT + BUTTON_HEIGHT;

    bool isExit = false;
    SDL_Event menuEvent;
    menuEvent.type = 1;
    while (!isExit) {
        renderMenu(render, txr_menu, txr_new_gameb, txr_settingsb, txr_statisticb, txr_exitb);
        while (SDL_PollEvent(&menuEvent)) {
            if (menuEvent.type == SDL_QUIT) {
                isExit = true;
            }
            if (menuEvent.button.x >= newGame.xLeft && menuEvent.button.x <= newGame.xRight // new game
                && menuEvent.button.y >= newGame.yHigh && menuEvent.button.y <= newGame.yLow) {
                renderMenu(render, txr_menu, txr_new_gamer, txr_settingsb, txr_statisticb, txr_exitb);
            }
            if (menuEvent.button.x >= newGame.xLeft && menuEvent.button.x <= newGame.xRight // new game
                && menuEvent.button.y >= newGame.yHigh && menuEvent.button.y <= newGame.yLow
                && menuEvent.type == SDL_MOUSEBUTTONDOWN && menuEvent.button.button == SDL_BUTTON_LEFT) {
                isExit = true;
                Mix_PauseMusic();
                break;
            }
            if (menuEvent.button.x >= setting.xLeft && menuEvent.button.x <= setting.xRight // setting
                && menuEvent.button.y >= setting.yHigh && menuEvent.button.y <= setting.yLow) {
                renderMenu(render, txr_menu, txr_new_gameb, txr_settingsr, txr_statisticb, txr_exitb);
            }
            if (menuEvent.button.x >= setting.xLeft && menuEvent.button.x <= setting.xRight // setting
                && menuEvent.button.y >= setting.yHigh && menuEvent.button.y <= setting.yLow
                && menuEvent.type == SDL_MOUSEBUTTONDOWN && menuEvent.button.button == SDL_BUTTON_LEFT) {
                // TODO: setting 
            }
            if (menuEvent.button.x >= statistics.xLeft && menuEvent.button.x <= statistics.xRight // statistics
                && menuEvent.button.y >= statistics.yHigh && menuEvent.button.y <= statistics.yLow) {
                renderMenu(render, txr_menu, txr_new_gameb, txr_settingsb, txr_statisticr, txr_exitb);
            }
            if (menuEvent.button.x >= statistics.xLeft && menuEvent.button.x <= statistics.xRight // statistics
                && menuEvent.button.y >= statistics.yHigh && menuEvent.button.y <= statistics.yLow
                && menuEvent.type == SDL_MOUSEBUTTONDOWN && menuEvent.button.button == SDL_BUTTON_LEFT) {
                // TODO: statistics 
            }
            if (menuEvent.button.x >= exit.xLeft && menuEvent.button.x <= exit.xRight // exit
                && menuEvent.button.y >= exit.yHigh && menuEvent.button.y <= exit.yLow) {
                renderMenu(render, txr_menu, txr_new_gameb, txr_settingsb, txr_statisticb, txr_exitr);
            }
            if (menuEvent.button.x >= exit.xLeft && menuEvent.button.x <= exit.xRight // exit
                && menuEvent.button.y >= exit.yHigh && menuEvent.button.y <= exit.yLow
                && menuEvent.type == SDL_MOUSEBUTTONDOWN && menuEvent.button.button == SDL_BUTTON_LEFT) {
                while (SDL_WaitEvent(&menuEvent)) {
                    if (menuEvent.type == SDL_QUIT) {
                        break;
                    }
                    gameOver(render, txr_game_over);
                }
                return 0;
            }
        }
    }
*/

    auto txtr_field = sdl_core::TextureManager::load_texture(
            tetris::field::PATH_TXTR, window.get_render()
            ); 
    auto txtr_ttr = sdl_core::TextureManager::load_texture(
        tetris::ttr::PATH_TXTR, window.get_render()
        ); // TODO: all loading - partional threads
    bool is_2_players = false; 
    
    if (is_2_players) {
        window.set_width(window.get_width() * 2);

//        std::thread second_player(
//            tetris::game_proc::game_loop, std::ref(window), 
//            std::ref(txtr_field), std::ref(txtr_ttr), tetris::Players::SECOND 
//        );
        tetris::game_proc::game_loop(
            window, txtr_field.get(), txtr_ttr.get(), tetris::Players::FIRST
            );
//        second_player.join();
    } else {
        tetris::game_proc::game_loop(
            window, txtr_field.get(), txtr_ttr.get(), tetris::Players::FIRST
            );
    }
    
/*    gameOver(render, texGameOver);

    SDL_Event end = menuEvent;
    SDL_Event temp;
    SDL_Event general;
    
    while (SDL_WaitEvent(&end)) {
        gameOver(render, txr_game_over);
        if (end.type == SDL_QUIT) {
            break;
        }
    }
    Statistic statistic;
    statistic = getStatistic();
    std::cout << statistic.time << "\n";
    std::cout << "Poi32s: " << statistic.poi32;
*/
    
    return 0;
} 








