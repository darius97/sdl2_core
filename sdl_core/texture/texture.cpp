#include "texture.hpp"

#include "../../types.hpp"

#include <string>
#include <memory>
#include <stdio.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
//#include <SDL2/SDL_ttf.h>
namespace sdl_core { 

    SDL_Rect TextureManager::make_rect(
        const i32 x, const i32 y, const ui32 w, const ui32 h
        ) {
        SDL_Rect rect;
        rect.x = x;
        rect.y = y;
        rect.w = w;
        rect.h = h;
        return rect;
    }

    SDL_Rect TextureManager::make_rect(
        const i32 x, const i32 y, SDL_Texture *texture
        ) {
        SDL_Rect rect_;
        rect_.x = x;
        rect_.y = y;
        SDL_QueryTexture(texture, NULL, NULL, &rect_.w, &rect_.h);
        return rect_;
    }

    void TextureManager::render(
        const i32 x, const i32 y, SDL_Texture *texture, SDL_Renderer *render
        ) {
        SDL_Rect render_position = make_rect(x, y, texture);
        SDL_RenderCopy(render, texture, NULL, &render_position);
    }

    void TextureManager::render(
        const i32 x, const i32 y, const ui32 w, const ui32 h,
        SDL_Texture *texture, SDL_Renderer *render
        ) {
        SDL_Rect render_position = make_rect(x, y, w, h);
        SDL_RenderCopy(render, texture, NULL, &render_position);
    }

    void TextureManager::render(
        SDL_Rect *rendering, SDL_Texture *texture, SDL_Renderer *render
        ) {
        SDL_RenderCopy(render, texture, NULL, rendering);
    }

    void TextureManager::render(
        const i32 x, const i32 y, const ui32 w, const ui32 h,
        SDL_Texture *texture, SDL_Renderer *render, SDL_Rect *clipping
        ) {
        SDL_Rect target  = make_rect(x, y, w, h);
        SDL_RenderCopy(render, texture, clipping, &target);
    }

    void TextureManager::render(
        const i32 x, const i32 y,
        const ui32 w, const ui32 h,
        SDL_Texture *texture, SDL_Renderer *render,
        const i32 textr_coordx, const i32 textr_coordy,
        const ui32 textr_w, const ui32 textr_h
        ) { 
        SDL_Rect clipping = make_rect(
            textr_coordx, textr_coordy, textr_w, textr_h
            );
        SDL_Rect target  = make_rect(x, y, w, h);
        SDL_RenderCopy(render, texture, &clipping, &target);
    }

    /*TextManager::TextManager(const std::string &path_font, ui32 font_size) {
        if (TTF_Init() != 0) {
            fprintf(stderr, "SDL2_ttf: can't init");
        }
        mfont = TTF_OpenFont(path_font.c_str(), font_size);
        if (mfont == nullptr) {
            fprintf(stderr, "TTF_OpenFont: can't open font file");
        }   
    }

    TextManager::~TextManager() {
        TTF_CloseFont(mfont);
    }

    SDL_Texture* TextManager::load_text(
            const std::string &text, SDL_Renderer *render
        ) {
        SDL_Color color = { 0, 0, 0, 255 };

        SDL_Surface *surface = TTF_RenderText_Blended(mfont, text.c_str(), color);
        if (surface == nullptr){
            TTF_CloseFont(mfont);
            fprintf(stderr, "TTF_RenderText: can't make surface");
            return nullptr;
        }  
        SDL_Texture *texture = SDL_CreateTextureFromSurface(render, surface);
        if (texture == nullptr){
            fprintf(stderr, "TTF_RenderText: can't make texture");
        }
        SDL_FreeSurface(surface);
        return texture;
    } */

} 