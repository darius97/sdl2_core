#ifndef _TEXTURE_
#define _TEXTURE_ 


#include "../../types.hpp"
#include "../../macro.hpp"

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

#include <string>
#include <memory>

namespace sdl_core {

    namespace {
        
        auto deleter = [](SDL_Texture *ptxtr) {
            SDL_DestroyTexture(ptxtr);
        };

    }

    class TextureManager {
    public:
        TextureManager() = default;
        ~TextureManager() = default;
        
        NO_COPY_NO_MOVE(TextureManager)
        
        static std::unique_ptr<SDL_Texture, decltype(deleter)> load_texture(
            const std::string &path_file, SDL_Renderer *render
            ) {
            std::unique_ptr<SDL_Texture, decltype(deleter)> 
                txtr(IMG_LoadTexture(render, path_file.c_str()), deleter);

            if (txtr.get() == nullptr) {
                fprintf(stderr, "can't load texture '%s'\n", path_file.c_str());
            }
            return txtr;
        }

        static SDL_Rect make_rect(
            const i32 x, const i32 y, const ui32 w, const ui32 h
            );
        
        static SDL_Rect make_rect(const i32 x, const i32 y, SDL_Texture *texture);
                
        static void render(
            const i32 x, const i32 y, SDL_Texture *texture, SDL_Renderer *render
            );

        static void render(
            const i32 x, const i32 y, const ui32 w, const ui32 h,
            SDL_Texture *texture, SDL_Renderer *render
            );

        static void render(
            SDL_Rect *rendering, SDL_Texture *texture, SDL_Renderer *render
        );
        
        static void render(
            const i32 x, const i32 y, const ui32 w, const ui32 h,
            SDL_Texture *texture, SDL_Renderer *render, SDL_Rect *clipping
            );

        static void render(
            const i32 x, const i32 y, const ui32 w, const ui32 h,
            SDL_Texture *texture, SDL_Renderer *render, 
            const i32 textr_coordx, const i32 textr_coordy,
            const ui32 textr_w, const ui32 textr_h
            );

    };

    /*class TextManager {
    public:
        TextManager(
            const std::string &path_font = "../bolditalic.ttf", ui32 font_size = 55
            );
        ~TextManager();

        NO_COPY_NO_MOVE(TextManager)

        SDL_Texture* load_text(const std::string &text, SDL_Renderer *render);

    private:
        TTF_Font *mfont;
    }; */

}

#endif 