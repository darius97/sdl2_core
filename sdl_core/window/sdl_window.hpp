#ifndef _SDL_WINDOW_
#define _SDL_WINDOW_


#include "../../types.hpp"
#include "../../macro.hpp"

#include <cassert>
#include <string>
#include <memory>

#include <SDL2/SDL.h>

namespace sdl_core { 

    namespace {
            
        auto window_deleter = [](SDL_Window *window) {
            SDL_DestroyWindow(window);
        };

        auto render_deleter = [](SDL_Renderer *render) {
            SDL_DestroyRenderer(render);
        };

    }

    class SDLWindow {
    public:
        SDLWindow(const std::string &title, const ui32 w, const ui32 h,
            const ui32 x = SDL_WINDOWPOS_CENTERED, 
            const ui32 y = SDL_WINDOWPOS_CENTERED
            );
        
        ~SDLWindow();
        
        NO_COPY_NO_MOVE(SDLWindow)

        ui32 get_width() const { return mw; }
        void set_width(const ui32 width) {
            mw = width;
            SDL_SetWindowSize(mwindow.get(), mw, mh);
        }
        
        ui32 get_height() const { return mh; }
        void set_height(const ui32 height) {
            mh = height;
            SDL_SetWindowSize(mwindow.get(), mw, mh);
        }
        
        ui32 get_x() const { return mx; }
        ui32 get_y() const { return my; }
        
        SDL_Window* get_window() const { return mwindow.get(); }
        SDL_Renderer* get_render() const { return mrender.get(); }

    private:
        ui32 mw, mh;
        ui32 mx, my;

        std::unique_ptr<SDL_Window, decltype(window_deleter)> mwindow;
        std::unique_ptr<SDL_Renderer, decltype(render_deleter)> mrender;
    };

}

#endif 