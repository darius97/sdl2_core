#include "sdl_window.hpp"

#include <cassert>
#include <string>
#include <memory>

#include <SDL2/SDL.h>
//#include <GL/gl.h>
//#include <GL/glu.h>

namespace sdl_core { 

    SDLWindow::SDLWindow(
        const std::string &title, 
        const ui32 w, const ui32 h, const ui32 x, const ui32 y
        ) : mw{ w }, mh{ h }, mx{ x }, my{ y }, 
        mwindow{ nullptr, window_deleter }, mrender{ nullptr, render_deleter } {
        
        assert(SDL_Init(SDL_INIT_EVERYTHING) == 0);

        mwindow.reset(SDL_CreateWindow(
            title.c_str(), mx, my,  mw, mh, SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL
            ));
        assert(mwindow != nullptr);
            
        mrender.reset(SDL_CreateRenderer(
            mwindow.get(), -1, 
            SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC
            ));
        assert(mrender != nullptr);
    }

    SDLWindow::~SDLWindow() {
        SDL_Quit();
    } 

} 