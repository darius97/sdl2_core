#ifndef _SDL_TIMER_
#define _SDL_TIMER_


#include "../../macro.hpp"
#include "../../types.hpp"


#include <SDL2/SDL.h>

class Timer {
public:
    Timer(const ui32 start_time) 
        : mstart_time { start_time }, maccum_time { 0 }, 
        mpassed_cycle_time { 0 } {}

    Timer() 
        : mstart_time { SDL_GetTicks() }, maccum_time { 0 }, 
        mpassed_cycle_time { 0 } {}
    
    ~Timer() = default;

    static ui32 get_ticks() { return SDL_GetTicks(); }

    void update_accum_time(const ui32 accum_time) { maccum_time += accum_time; }
    void update_accum_time() { maccum_time += mpassed_cycle_time; }

    void drop_accum() { maccum_time = 0; }
    ui32 get_accum() { return maccum_time; }

    void set_start_time(const ui32 start_time) { mstart_time = start_time; }
    ui32 get_start_time() { return mstart_time; }
    
    void calc_passed_cycle_time() {
        mpassed_cycle_time = get_ticks() - mstart_time - maccum_time;
    }
    ui32 get_passed_cycle_time() { return mpassed_cycle_time; }
    
    NO_COPY_NO_MOVE(Timer)

private:
    ui32 mstart_time, maccum_time, mpassed_cycle_time;
};

#endif 