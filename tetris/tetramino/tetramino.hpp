#ifndef _TETRIS_TETRAMINO_
#define _TETRIS_TETRAMINO_


#include "../../types.hpp"
#include "../../math/vec2d.hpp"

#include "../param.hpp"

#include <string>
#include <array>

#include <SDL2/SDL.h>

namespace tetris {

    namespace ttr {
        
        namespace cube {

            constexpr i32 SIZE = 64;
            constexpr i32 COUNT_PER_TTR = 4;

        }
        
        const std::string PATH_TXTR
        = "tetris/media/image/tetramino.png";

        constexpr i32 WIDTH = 2;
        constexpr i32 HEIGHT = 4;
        
        constexpr i32 TYPE_COUNT = 7;
        
        constexpr i32 TETRAMINOES[TYPE_COUNT][HEIGHT] = {
            { 3, 5, 4, 6 }, // Z
            { 2, 3, 5, 7 }, // L
            { 2, 3, 4, 5 }, // O
            { 2, 4, 5, 7 }, // S
            { 1, 3, 5, 7 }, // I
            { 3, 5, 7, 6 }, // J
            { 3, 5, 4, 7 }  // T
        };
        
        struct Tetramino {
            std::array<Vec2i, cube::COUNT_PER_TTR> coords;
            ui32 type = 0;
        };

        ui32 generate_type();

        inline bool is_cell_full(
            const Vec2i &position, 
            field::Cell game_field [field::HEIGHT][field::WIDTH]
            );

        void shift_left(
            std::array<Vec2i, cube::COUNT_PER_TTR> &ttr_coord,
            field::Cell game_field [field::HEIGHT][field::WIDTH]
        );
        
        void shift_right(
            std::array<Vec2i, cube::COUNT_PER_TTR> &ttr_coord, 
            field::Cell game_field [field::HEIGHT][field::WIDTH]
        ); 
        
        void shift_down(
            std::array<Vec2i, cube::COUNT_PER_TTR> &ttr_coord, 
            field::Cell game_field [field::HEIGHT][field::WIDTH]
        );

        void rotation(
            std::array<Vec2i, cube::COUNT_PER_TTR> &ttr_coord, 
            field::Cell game_field [field::HEIGHT][field::WIDTH]
        );
        
        bool update(
            Tetramino &ttr, 
            field::Cell game_field [field::HEIGHT][field::WIDTH]
            );
        
        void translate_to_world_coords(Tetramino &ttr);
        
    }

}

#endif 