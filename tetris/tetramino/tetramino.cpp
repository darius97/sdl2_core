#include "tetramino.hpp"

#include "../../types.hpp"
#include "../param.hpp"

#include "../../math/vec2d.hpp"

#include <SDL2/SDL.h>

#include <array>
#include <ctime>
//#include <algorithm>

namespace tetris {
    
    namespace ttr {

        // use only in this scope
        bool is_ttr_fallen(
            Tetramino &ttr, field::Cell game_field [field::HEIGHT][field::WIDTH]
            );
        
        void check_field_fully_lines(
            field::Cell game_field [field::HEIGHT][field::WIDTH]
        );
            
        inline bool is_world_edge_x(const Vec2i &position);
        inline bool is_world_edge_y(const Vec2i &position);
        inline bool is_world_edge(const Vec2i &position);

        // use only in this scope
        
        void translate_to_world_coords(Tetramino &ttr) {
            for (i32 i = 0; i < cube::COUNT_PER_TTR; ++i) {
                ttr.coords[i].set_x(
                    TETRAMINOES[ttr.type][i] % WIDTH + (field::WIDTH / 2)
                    );
                ttr.coords[i].set_y(TETRAMINOES[ttr.type][i] / WIDTH - 1);
            }
        }

        ui32 generate_type() {
            srand(time(NULL));
            return rand() % TYPE_COUNT;
        }
        
        bool update(
            Tetramino &ttr, 
            field::Cell game_field [field::HEIGHT][field::WIDTH]
            ) {
            bool is_fallen = is_ttr_fallen(ttr, game_field);
            check_field_fully_lines(game_field);
            return is_fallen;
        }

        bool is_cell_full(
            const Vec2i &ttr_coord, 
            field::Cell game_field [field::HEIGHT][field::WIDTH]
            ) {
            return game_field[ttr_coord.get_y()][ttr_coord.get_x()].is_full == true;
        }

        bool is_world_edge_x(const Vec2i &ttr_coord) {
            return ttr_coord.get_x() < 0 
                || ttr_coord.get_x() >= field::WIDTH; 
        }
        bool is_world_edge_y(const Vec2i &ttr_coord) {
            return ttr_coord.get_y() < 0 
                || ttr_coord.get_y() >= field::HEIGHT;
        }

        bool is_world_edge(const Vec2i &ttr_coord) {
            return is_world_edge_x(ttr_coord) || is_world_edge_y(ttr_coord);
        }
            
        
        void shift_right(
            std::array<Vec2i, cube::COUNT_PER_TTR> &ttr_coord,
            field::Cell game_field [field::HEIGHT][field::WIDTH]
            ) {
            std::array<Vec2i, cube::COUNT_PER_TTR> save_ttr_coord = ttr_coord;
            
            for (i32 i = 0; i < cube::COUNT_PER_TTR; ++i) {
                ttr_coord[i].set_x(ttr_coord[i].get_x() + 1);
                
                if (is_world_edge_x(ttr_coord[i]) 
                || is_cell_full(ttr_coord[i], game_field)) {
                    ttr_coord = save_ttr_coord;
                    break;
                }
            }
        }

        void shift_left(
            std::array<Vec2i, cube::COUNT_PER_TTR> &ttr_coord,
            field::Cell game_field [field::HEIGHT][field::WIDTH]
            ) {
            std::array<Vec2i, cube::COUNT_PER_TTR> save_ttr_coord = ttr_coord;
            
            for (i32 i = 0; i < cube::COUNT_PER_TTR; ++i) {
                ttr_coord[i].set_x(ttr_coord[i].get_x() - 1);
                
                if (is_world_edge_x(ttr_coord[i]) 
                || is_cell_full(ttr_coord[i], game_field)) {
                    ttr_coord = save_ttr_coord;
                    break;
                }
            }
        }
        
        void shift_down(
            std::array<Vec2i, cube::COUNT_PER_TTR> &ttr_coord, 
            field::Cell game_field [field::HEIGHT][field::WIDTH]
            ) {
            std::array<Vec2i, cube::COUNT_PER_TTR> save_ttr_coord = ttr_coord;

            for (i32 i = 0; i < cube::COUNT_PER_TTR; ++i) {
                ttr_coord[i].set_y(ttr_coord[i].get_y() + 1);
                
                if (is_world_edge_y(ttr_coord[i])
                || is_cell_full(ttr_coord[i], game_field)) {
                    ttr_coord = save_ttr_coord;
                    break;
                }
            }
        }

        void rotation(
            std::array<Vec2i, cube::COUNT_PER_TTR> &ttr_coord, 
            field::Cell game_field [field::HEIGHT][field::WIDTH]
            ) {
            std::array<Vec2i, cube::COUNT_PER_TTR> save_ttr_coord = ttr_coord;
            
            const ui32 ROT_CENTER = 1;  // second cube - center of rotation
            for (i32 i = 0; i < cube::COUNT_PER_TTR; ++i) {
                ttr_coord[i].set_x(
                    save_ttr_coord[ROT_CENTER].get_x() 
                    - (save_ttr_coord[i].get_y() - save_ttr_coord[ROT_CENTER].get_y())
                    );
                ttr_coord[i].set_y(
                    save_ttr_coord[ROT_CENTER].get_y() 
                    + (save_ttr_coord[i].get_x() - save_ttr_coord[ROT_CENTER].get_x())
                    ); 
                
                if (is_world_edge(ttr_coord[i]) 
                || is_cell_full(ttr_coord[i], game_field)) {
                    ttr_coord = save_ttr_coord;
                    break;
                }
            }
        } 

        bool is_line_full(
            const i32 i_row, 
            field::Cell game_field [field::HEIGHT][field::WIDTH]
            ) {
            for (i32 j = 0; j < field::WIDTH; ++j) {
                if (game_field[i_row][j].is_full != true) {
                    return false;
                }
            }
            return true;
        }
        
        void delete_lines_start_i(
            const i32 start_delete, 
            field::Cell game_field [field::HEIGHT][field::WIDTH]
            ) {
            for (i32 i = start_delete; i > 0; --i) {
                for (i32 j = 0; j < field::WIDTH; ++j) {
                    game_field[i][j] = game_field[i - 1][j];
                }
            }           
        }
    //    i32 point = 0;
        void check_field_fully_lines(
            field::Cell game_field [field::HEIGHT][field::WIDTH]
            ) {
            bool is_line_full_ = true;
    //        i32 count = 0;
            
            for (i32 i = 0; i < field::HEIGHT; ++i) {
                is_line_full_ = is_line_full(i, game_field);
                if (is_line_full_) {
    //                ++count;
    //                point += parametrs::game::PRIZE_POINT * count;
                    delete_lines_start_i(i, game_field);
                }
    //            count = 0;
                is_line_full_ = true;
            }
        }

        void save_ttr_to_field(
            Tetramino &ttr, 
            field::Cell game_field [field::HEIGHT][field::WIDTH]
            ) {
            for (i32 i = 0; i < cube::COUNT_PER_TTR; ++i) {
                game_field[ttr.coords[i].get_y()][ttr.coords[i].get_x()]
                .is_full = true;
                game_field[ttr.coords[i].get_y()][ttr.coords[i].get_x()]
                .type = ttr.type;
            }
        } 

        bool is_ttr_fallen(
            Tetramino &ttr, 
            field::Cell game_field [field::HEIGHT][field::WIDTH]
            ) {
            std::array<Vec2i, cube::COUNT_PER_TTR> save_ttr_coord = ttr.coords;
            for (i32 i = 0; i < cube::COUNT_PER_TTR; ++i) {
                ttr.coords[i].set_y(ttr.coords[i].get_y() + 1);
                if (is_world_edge_y(ttr.coords[i]) 
                || is_cell_full(ttr.coords[i], game_field)
                ) {
                    ttr.coords = save_ttr_coord;
                    save_ttr_to_field(ttr, game_field);
                    return true;
                }
            }

            return false;
        } 
        
    } 

} 