#include "game_proc.hpp"

#include "../../types.hpp"

#include "../../sdl_core/timer/timer.hpp"

#include "../param.hpp"
#include "../render/render.hpp"


namespace tetris { 
    
    namespace game_proc {
        
        bool is_game_over(
            const field::Cell game_field [field::HEIGHT][field::WIDTH]
            ) { // check top line of game field
            for (i32 i = 0; i < field::WIDTH; ++i) {
                if (game_field[0][i].is_full == true) {
                    return true;
                }
            }
            return false;
        }
        
        void game_loop(
            const sdl_core::SDLWindow &window, SDL_Texture *txtr_field, 
            SDL_Texture *txtr_ttr, const Players player
            ) {
            ttr::Tetramino current_ttr;
            ttr::translate_to_world_coords(current_ttr);
            
            field::Cell game_field[field::HEIGHT][field::WIDTH];
            
            bool is_new_ttr = true;
            
            SDL_Event event;
            bool is_quit = false;
            
            Timer timer;
            while (!is_quit) {
                if (is_game_over(game_field)) {
                    is_quit = true;
                    break;
                }
                
                timer.calc_passed_cycle_time();
                if (timer.get_passed_cycle_time() > settings::DELAY_TTR_FALL) {
                    is_new_ttr = ttr::update(current_ttr, game_field);
                    timer.update_accum_time();

                    if (is_new_ttr) {
                        current_ttr.type = ttr::generate_type();
                        ttr::translate_to_world_coords(current_ttr);
                    }
                }
                
                event_proc(&event, is_quit, current_ttr, game_field, player);
                        
                GameRender::render(
                    window.get_render(), txtr_field, txtr_ttr,  
                    current_ttr, game_field, player
                    );
            }
        }

        void event_proc(
            SDL_Event *event, bool &is_quit, ttr::Tetramino &current_ttr, 
            field::Cell game_field[field::HEIGHT][field::WIDTH],
            const Players player
            ) {
            while (SDL_PollEvent(event)) {
                if (event->type == SDL_QUIT) {
                    is_quit = true;
                    break;
                }
                if (event->type == SDL_KEYDOWN) {
                    switch (player) {
                    case Players::FIRST:
                        keyboard_proc_player1(event, current_ttr, game_field);
                        break;
                    case Players::SECOND:
                        keyboard_proc_player2(event, current_ttr, game_field);
                        break;
                    default:
                        break;
                    }
                }
            }
        }

        void keyboard_proc_player1(
            SDL_Event *event, ttr::Tetramino &current_ttr,
            field::Cell game_field[field::HEIGHT][field::WIDTH]
            ) {
            switch (event->key.keysym.sym) {
                case SDLK_ESCAPE:
                    // TODO: enter to menu from game 
                    break;
                case SDLK_d:
                    ttr::shift_right(current_ttr.coords, game_field);
                    break;
                case SDLK_a:
                    ttr::shift_left(current_ttr.coords, game_field);
                    break;
                case SDLK_s:
                    ttr::shift_down(current_ttr.coords, game_field);
                    break;
                case SDLK_TAB:
                    ttr::rotation(current_ttr.coords, game_field);
                    break;
                default:
                    break;
            }
        } 

        void keyboard_proc_player2(
            SDL_Event *event, ttr::Tetramino &current_ttr,
            field::Cell game_field[field::HEIGHT][field::WIDTH]
            ) {
            switch (event->key.keysym.sym) {
                case SDLK_ESCAPE:
                    // TODO: enter to menu from game 
                    break;
                case SDLK_RIGHT:
                    ttr::shift_right(current_ttr.coords, game_field);
                    break;
                case SDLK_LEFT:
                    ttr::shift_left(current_ttr.coords, game_field);
                    break;
                case SDLK_DOWN:
                    ttr::shift_down(current_ttr.coords, game_field);
                    break;
                case SDLK_RCTRL:
                    ttr::rotation(current_ttr.coords, game_field);
                    break;
                default:
                    break;
            }
        } 

    }
    
} 