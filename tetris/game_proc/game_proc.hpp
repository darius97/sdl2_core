#ifndef _TETRIS_GAME_
#define _TETRIS_GAME_


#include "../../sdl_core/window/sdl_window.hpp"

#include "../tetramino/tetramino.hpp"

#include <SDL2/SDL.h>

namespace tetris { 

    namespace game_proc {

        void keyboard_proc_player1(
            SDL_Event *event, ttr::Tetramino &current_ttr,
            field::Cell game_field[field::HEIGHT][field::WIDTH]
            );

        void keyboard_proc_player2(
            SDL_Event *event, ttr::Tetramino &current_ttr,
            field::Cell game_field[field::HEIGHT][field::WIDTH]
            );

        void event_proc(
            SDL_Event *event, bool &is_quit, ttr::Tetramino &current_ttr, 
            field::Cell game_field[field::HEIGHT][field::WIDTH],
            const Players player
            );

        void game_loop(
            const sdl_core::SDLWindow &window, SDL_Texture *txtr_field, 
            SDL_Texture *txtr_ttr, const Players player
            );

        
        bool is_game_over(
            const field::Cell game_field [field::HEIGHT][field::WIDTH]
        );

    }
    
} 

#endif 