#ifndef _TETRIS_RENDER_
#define _TETRIS_RENDER_


#include "../../types.hpp"
#include "../../macro.hpp"

#include "../../sdl_core/texture/texture.hpp"

#include "../param.hpp"
#include "../tetramino/tetramino.hpp"

#include <SDL2/SDL.h>

namespace tetris { 
    
    const std::string pathGameOver = "image/gameOver.png";
    
    class GameRender {
    public:
        static void render(
            SDL_Renderer *render, SDL_Texture *txtr_background, 
            SDL_Texture *txtr_ttr, const ttr::Tetramino &current_ttr, 
            const field::Cell game_field [field::HEIGHT][field::WIDTH], 
            const tetris::Players player
        );

    private:
        static sdl_core::TextureManager mtxtr_manager;

        static void render_current_tetramino(
            SDL_Renderer *render, SDL_Texture *txtr_ttr, 
            const ttr::Tetramino &current_ttr, const Players player
        );
        
        static void render_field(
            SDL_Renderer *render, SDL_Texture *txtr_ttr,
            const field::Cell game_field [field::HEIGHT][field::WIDTH], 
            const Players player
        );

        static void render_background(
            SDL_Renderer *render, SDL_Texture *txtr_background,
            const Players player
        );

    };

}

#endif 