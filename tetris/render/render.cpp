#include "render.hpp"

#include "../../types.hpp"
#include "../param.hpp"
#include "../tetramino/tetramino.hpp"

#include <SDL2/SDL.h>

namespace tetris { 
    
    void GameRender::render(
        SDL_Renderer *render, SDL_Texture *txtr_background, 
        SDL_Texture *txtr_ttr, const ttr::Tetramino &current_ttr, 
        const field::Cell game_field [field::HEIGHT][field::WIDTH], 
        const tetris::Players player
        ) {
        SDL_RenderClear(render);
       
        render_background(render, txtr_background, player); 
        render_field(render, txtr_ttr, game_field, player);
        render_current_tetramino(render, txtr_ttr, current_ttr, player);
        
        SDL_RenderPresent(render);  
    }

    void GameRender::render_current_tetramino(
        SDL_Renderer *render, SDL_Texture *txtr_ttr, 
        const ttr::Tetramino &current_ttr, const Players player
        ) {
        SDL_Rect clipping = mtxtr_manager.make_rect(
            current_ttr.type * ttr::cube::SIZE, 0, 
            ttr::cube::SIZE, ttr::cube::SIZE
        );
                    
        for (i32 i = 0; i < ttr::HEIGHT; ++i) {
            if (ttr::TETRAMINOES[current_ttr.type][i]) {
                mtxtr_manager.render(
                    current_ttr.coords[i].get_x() * ttr::cube::SIZE 
                    + static_cast<int>(player) * window::WIDTH,
                    current_ttr.coords[i].get_y() * ttr::cube::SIZE,
                    ttr::cube::SIZE, ttr::cube::SIZE,
                    txtr_ttr, render, &clipping
                );
            }
        }
    }

    void GameRender::render_field(
        SDL_Renderer *render, SDL_Texture *txtr_ttr,
        const field::Cell game_field [field::HEIGHT][field::WIDTH], 
        const Players players
        ) {
        for (i32 i = 0; i < field::HEIGHT; ++i) {
            for (i32 j = 0; j < field::WIDTH; ++j) {
                if (game_field[i][j].is_full == true) {
                    SDL_Rect clipping = mtxtr_manager.make_rect(
                        game_field[i][j].type * ttr::cube::SIZE, 0, 
                        ttr::cube::SIZE, ttr::cube::SIZE
                    );
                    mtxtr_manager.render(
                        ttr::cube::SIZE * j 
                        + static_cast<int>(players) * window::WIDTH, 
                        ttr::cube::SIZE * i, 
                        ttr::cube::SIZE, ttr::cube::SIZE,
                        txtr_ttr, render, &clipping
                        );
                }
            }
        }
    }
    
    void GameRender::render_background(
        SDL_Renderer *render, SDL_Texture *txtr_background, const Players player
        ) {
        mtxtr_manager.render(
            static_cast<int>(player) * window::WIDTH, 0, 
            txtr_background, render
            );
    }

/*void gameOver(SDL_Renderer* render, SDL_Texture* texGameOver) {
        SDL_RenderClear(render);
        own::renderTexture(texGameOver, render, NULL, NULL);
        SDL_RenderPresent(render);
    } 
 */
} 