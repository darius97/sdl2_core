#ifndef _TETRIS_PARAM_
#define _TETRIS_PARAM_


#include "../types.hpp"
#include <string>


namespace tetris { 

    enum class Players {
        FIRST,
        SECOND
    };

    namespace field {

        const std::string PATH_TXTR = "tetris/media/image/background_black.png";

        constexpr i32 WIDTH = 9;
        constexpr i32 HEIGHT = 11;
        

        struct Cell {
            i32 type;
            bool is_full = false;
        };

    }
        
    namespace music {

        const std::string END_PATH = "music/podmoskovnye_vecera.mp3";
        const std::string GAME_PATH = "music/korobeiniki.mp3";

    }

    namespace window {

        constexpr i32 WIDTH = 576;
        constexpr i32 HEIGHT = 704;
        constexpr i32 BACKGROUND_WIDTH = 576;
        constexpr i32 BACKGROUND_HEIGHT = 704;

    }

    namespace settings {

        constexpr i32 DELAY_TTR_FALL = 1000;

        constexpr i32 PRIZE_POINT = 100;
        constexpr i32 DATA_SIZE = 29;

    }

    namespace menu {

        const std::string PATH_TXTR = "image/menu.png";

        namespace button {

            constexpr i32 WIDTH = 256;
            constexpr i32 HEIGHT = 64;
            constexpr i32 OFFSET_1 = 2;
            constexpr i32 OFFSET_2 = 4;
            constexpr i32 OFFSET_3 = 6;
            constexpr i32 OFFSET_4 = 8;
            constexpr i32 CENTER_POSITION_X_LEFT 
                = (window::WIDTH - WIDTH) / 2;
            constexpr i32 CENTER_POSITION_X_RIGHT 
                = (window::WIDTH - WIDTH) / 2 + WIDTH;

            const std::string pathNewGameB = "image/newGameB.png";
            const std::string pathNewGameR = "image/newGameR.png";
            const std::string pathSettingB = "image/settingB.png";
            const std::string pathSettingR = "image/settingR.png";
            const std::string pathStatisticsB = "image/statisticsB.png";
            const std::string pathStatisticsR = "image/statisticsR.png";
            const std::string pathExitB = "image/exitB.png";
            const std::string pathExitR = "image/exitR.png";
            
            struct Position {
                i32 xLeft;
                i32 xRight;
                i32 yHigh;
                i32 yLow;
            };

        }

    }

}

#endif 