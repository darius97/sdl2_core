#ifndef _VEC2D_
#define _VEC2D_


#include "vec2d.hpp"
#include "../types.hpp"
#include <cmath>

class Vec2i {
public:
    Vec2i(const i32 x, const i32 y) : mx{ x }, my{ y } {}
    Vec2i() : mx{ 1 }, my{ 1 } {}

    ~Vec2i() = default;
    
    i32 get_x() const { return mx; }
    i32 get_y() const { return my; }

    void set_x(const i32 x) { mx = x; }
    void set_y(const i32 y) { my = y; }

    const Vec2i& operator+() const { return *this; }
    Vec2i operator-() const { return Vec2i(-mx, -my); }
//    i32 operator[](i32 i) const { return mvec[i]; }
//    i32& operator[](i32 i) { return mvec[i]; }

    Vec2i& operator+=(const Vec2i &other);
    Vec2i& operator-=(const Vec2i &other);
    Vec2i& operator*=(const Vec2i &other);
    Vec2i& operator/=(const Vec2i &other);
    Vec2i& operator*=(const i32 t);
    Vec2i& operator/=(const i32 t);

    double length() const { return std::sqrt(mx * mx + my * my); }
    i32 squared_length() const { return mx * mx + my * my; }

    void normalize();
    Vec2i get_normalize();

    double dot(const Vec2i &v);

private:
    i32 mx, my;
};

double dot(const Vec2i &v1, const Vec2i &v2);
Vec2i get_normalize(const Vec2i &v);
Vec2i operator+(const Vec2i &v1, const Vec2i &v2);
Vec2i operator-(const Vec2i &v1, const Vec2i &v2);
Vec2i operator*(const i32 k, const Vec2i &v);
Vec2i operator*(const Vec2i &v, const i32 k);
Vec2i operator*(const Vec2i &v1, const Vec2i &v2);
Vec2i operator/(const Vec2i &v, const i32 k);

#endif 