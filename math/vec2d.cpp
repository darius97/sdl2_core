#include "vec2d.hpp"
#include <cmath>


void Vec2i::normalize() {
    i32 k = 1.0 / sqrt(mx * mx + my * my);
    mx *= k; my *= k;
}

Vec2i Vec2i::get_normalize() {
    i32 ln = this->length();
    return Vec2i{ mx / ln, my / ln };
}

double Vec2i::dot(const Vec2i &v) {
    return mx * v.mx + my * v.my;
}

Vec2i& Vec2i::operator+=(const Vec2i &v) {
    mx += v.mx;
    my += v.my;
    return *this;
}

Vec2i& Vec2i::operator-=(const Vec2i& v) {
    mx -= v.mx;
    my -= v.my;
    return *this;
}

Vec2i& Vec2i::operator*=(const i32 t) {
    mx *= t;
    my *= t;
    return *this;
}

Vec2i& Vec2i::operator/=(const i32 t) {
    i32 k = 1.0 / t;
    mx *= k;
    my *= k;
    return *this;
}

double dot(const Vec2i &v1, const Vec2i &v2) {
    return v1.get_x() *v2.get_x() + v1.get_y() *v2.get_y();
}

Vec2i operator+(const Vec2i &v1, const Vec2i &v2) {
    return Vec2i{ v1.get_x() + v2.get_x(), v1.get_y() + v2.get_y() };
}

Vec2i operator-(const Vec2i &v1, const Vec2i &v2) {
    return Vec2i{ v1.get_x() - v2.get_x(), v1.get_y() - v2.get_y() };
}

Vec2i operator*(const i32 k, const Vec2i &v) {
    return Vec2i{ k * v.get_x(), k * v.get_y() };
}

Vec2i operator*(const Vec2i &v1, const Vec2i &v2) {
    return Vec2i{ v1.get_x() * v2.get_x(), v1.get_y() * v2.get_y() };
}

Vec2i operator*(const Vec2i &v, const i32 k) {
    return Vec2i{ k * v.get_x(), k * v.get_y() };
}

Vec2i operator/(const Vec2i &v, const i32 k) {
    return Vec2i{ v.get_x() / k, v.get_y() / k  };
} 


Vec2i get_normalize(const Vec2i &v) {
    return v / v.length();
} 