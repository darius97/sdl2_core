CC = g++
FDEBUG = -Wall -g
FEXTRA = -pthread
SDL_DEP = -lSDL2 -lSDL2_image -lSDL2_ttf -lSDL2_mixer
CORE_DEP = sdl_core/window/sdl_window.o sdl_core/texture/texture.o
MATH_DEP = math/vec2d.o 
TETRIS_DEP = tetris/tetramino/tetramino.o tetris/render/render.o tetris/game_proc/game_proc.o
MAIN_DEP = $(CORE_DEP) $(MATH_DEP) $(TETRIS_DEP)

main: main.cpp $(MAIN_DEP)
	$(CC) $(FDEBUG) $^ $(SDL_DEP) -o $@